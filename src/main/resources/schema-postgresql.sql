CREATE TABLE IF NOT EXISTS USERS (
 id bigserial not null primary key,
 user_name varchar(100) not null,
 pass_word varchar(100) not null,
 first_name varchar(100) not null,
 last_name varchar(100) not null,
 access_id int not null
);

CREATE TABLE IF NOT EXISTS ACCESS (
 id bigserial not null primary key,
 name varchar(10) not null
);


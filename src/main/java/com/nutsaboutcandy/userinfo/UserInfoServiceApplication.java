package com.nutsaboutcandy.userinfo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.nutsaboutcandy.userinfo.entity.Access;
import com.nutsaboutcandy.userinfo.entity.User;
import com.nutsaboutcandy.userinfo.mapper.AccessMapper;
import com.nutsaboutcandy.userinfo.service.UserService;

@SpringBootApplication
public class UserInfoServiceApplication implements CommandLineRunner{

	public static void main(String[] args) {
		SpringApplication.run(UserInfoServiceApplication.class, args);
	}
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private AccessMapper accessMapper;
	
	@Override
	public void run(String... args) throws Exception {
		
		insertdata();
		
	}

	private void insertdata() {
		
		if(accessMapper.findAll().size() == 0) {
			long id = accessMapper.insert(new Access("ADMIN"));
			long id2 = accessMapper.insert(new Access("MEMBER"));
			
			if(userService.getAllUsers().size() == 0) {
				
				userService.add(new User("admin", passwordEncoder.encode("pass"), "Victor", "Avila", id));
				userService.add(new User("john", passwordEncoder.encode("pass"), "John", "Doe", id2));
				userService.add(new User("anna", passwordEncoder.encode("pass"), "Anna", "Smith", id2));
				userService.add(new User("kevin", passwordEncoder.encode("pass"), "Kevin", "Mace", id2));
			}
		}
	}

}

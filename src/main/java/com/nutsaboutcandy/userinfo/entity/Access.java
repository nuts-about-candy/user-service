package com.nutsaboutcandy.userinfo.entity;

public class Access {

	private long id;
	private String name;
	
	public Access() {}
	
	public Access(String name) {
		this.setName(name);
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Access [id=" + id + ", name=" + name + "]";
	}
	
	
}

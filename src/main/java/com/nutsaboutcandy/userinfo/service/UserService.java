package com.nutsaboutcandy.userinfo.service;

import java.util.List;

import com.nutsaboutcandy.userinfo.entity.User;

public interface UserService {

	User getUserByUserId(long id);
	
	List<User> getAllUsers();
	
	long add(User user);

	void signUp(User user);
}

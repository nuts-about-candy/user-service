package com.nutsaboutcandy.userinfo.service.impl;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.nutsaboutcandy.userinfo.entity.Access;
import com.nutsaboutcandy.userinfo.entity.User;
import com.nutsaboutcandy.userinfo.mapper.AccessMapper;
import com.nutsaboutcandy.userinfo.mapper.UserMapper;
import com.nutsaboutcandy.userinfo.security.MyUserDetails;
import com.nutsaboutcandy.userinfo.service.UserService;

@Service
public class UserServiceImpl implements UserService, UserDetailsService  {

	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private UserMapper userMapper;
	
	@Autowired
	private AccessMapper accessMapper;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		User user = userMapper.findUserByUserName(username);
		if(user != null) {
			User userDetails = userMapper.findById(user.getId());
			return new MyUserDetails(user.getPassword(), userDetails, 
					Arrays.asList(new SimpleGrantedAuthority(userDetails.getAccess())));
		}
		
		throw new UsernameNotFoundException("Invalid username and password.");
	}
	
	
	@Override
	public void signUp(User user) {

		if(user == null) 
			throw new IllegalArgumentException("Illegal arguments on signup.");
		
		if(userMapper.findUserByUserName(user.getUsername()) != null)
			throw new IllegalArgumentException("Username already exist.");
		
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		Access access = accessMapper.findAccessName("MEMBER");
		
		if(access != null) {
			user.setAccessId(access.getId());
			userMapper.insert(user);
		}
	}


	@Override
	public User getUserByUserId(long id) {
		
		User user = userMapper.findById(id);
		if(user != null) 
			return user;
			
		throw new RuntimeException(String.format("User with ID '%s' is not found.", id));
	}

	@Override
	public List<User> getAllUsers() {

		return userMapper.findAll();
	}

	@Override
	public long add(User user) {
		
		if(user == null) 
			throw new IllegalArgumentException("user cannot be null");
		
		return userMapper.insert(user);
	}

	
}

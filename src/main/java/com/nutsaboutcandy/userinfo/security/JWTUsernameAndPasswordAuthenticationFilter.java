package com.nutsaboutcandy.userinfo.security;

import java.io.IOException;
import java.sql.Date;
import java.time.LocalDate;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nutsaboutcandy.userinfo.jwt.JWTConfig;

import io.jsonwebtoken.Jwts;

public class JWTUsernameAndPasswordAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

	private final AuthenticationManager authenticationManager;
	private final JWTConfig config;

	public JWTUsernameAndPasswordAuthenticationFilter(AuthenticationManager authenticationManager, 
			JWTConfig config) {
		this.authenticationManager = authenticationManager;
		this.config = config;
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException {

		try {
			
			MyUserDetails authRequest = new ObjectMapper()
					.readValue(request.getInputStream(), MyUserDetails.class);

			Authentication authentication = new UsernamePasswordAuthenticationToken(authRequest.getUsername(),
					authRequest.getPassword());
			Authentication authenticate = authenticationManager.authenticate(authentication);

			return authenticate;

		} catch (IOException e) {
			throw new RuntimeException(e.getMessage());
		}
	}
	
	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
			Authentication authResult) throws IOException, ServletException {

		MyUserDetails details = (MyUserDetails) authResult.getPrincipal();
		
		String token = Jwts.builder()
				.setSubject(new ObjectMapper().writeValueAsString(details.getUser()))
				.claim(config.getAuthorizatiesKey(), authResult.getAuthorities())
				.setExpiration(Date.valueOf(LocalDate.now().plusDays(config.getTokenExpirationAfterDays())))
				.signWith(config.getSecretKeyForSigning())
				.compact();
		
		response.setContentType(MediaType.APPLICATION_JSON_VALUE);
		response.getWriter().write("{\"" + config.getAuthorization() + "\":\"Bearer " + token+ "\"}");
	}
}

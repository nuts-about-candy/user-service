package com.nutsaboutcandy.userinfo.security;


import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import com.nutsaboutcandy.userinfo.exception.RestAuthenticationEntryPoint;
import com.nutsaboutcandy.userinfo.jwt.JWTAuthorizationFilter;
import com.nutsaboutcandy.userinfo.jwt.JWTConfig;
import com.nutsaboutcandy.userinfo.service.impl.UserServiceImpl;

@Configuration
@EnableWebSecurity
public class ApplicationSecurityConfig extends WebSecurityConfigurerAdapter{

	private final UserServiceImpl userServiceImpl;
	private final PasswordEncoder passwordEncoder;
	private final JWTConfig config;
	
	@Autowired
	public ApplicationSecurityConfig(UserServiceImpl userServiceImpl,
			PasswordEncoder passwordEncoder, 
			JWTConfig config) {
		
		this.userServiceImpl = userServiceImpl;
		this.passwordEncoder = passwordEncoder;
		this.config = config;
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		http
		.cors()
		.and()
		.csrf()
			.disable()
		.sessionManagement()
			.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
		.and()
        .exceptionHandling()
        .authenticationEntryPoint(authenticationEntryPoint())
		.and()
		.addFilter(new JWTUsernameAndPasswordAuthenticationFilter(authenticationManager(), config))
		.addFilterAfter(new JWTAuthorizationFilter(config), UsernamePasswordAuthenticationFilter.class)
		.authorizeRequests()
			.antMatchers("/login").permitAll() 
			.antMatchers("/signup").permitAll()
			.antMatchers("/users/profile/*").hasAnyAuthority("MEMBER", "ADMIN")
			.antMatchers("/users*").hasAnyAuthority("ADMIN")
			.antMatchers("/admin/users*", "/admin/users/*").hasAnyAuthority("ADMIN")
		.anyRequest()
		.authenticated();
	}
	
	@Bean
    CorsConfigurationSource corsConfigurationSource() {
		
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(Arrays.asList("*"));
        configuration.setAllowedMethods(Arrays.asList("GET","POST", "DELETE", "PUT"));
        configuration.setAllowedHeaders(Arrays.asList("*"));
        configuration.setAllowCredentials(true);
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		
		auth.authenticationProvider(daoAuthenticationProvider());
	}
	
	@Bean
	public DaoAuthenticationProvider daoAuthenticationProvider() {
		
		DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
		provider.setPasswordEncoder(passwordEncoder);
		provider.setUserDetailsService(userServiceImpl);
		return provider;
	}
	
    @Bean
    RestAuthenticationEntryPoint authenticationEntryPoint() {
        return new RestAuthenticationEntryPoint();
    }

}

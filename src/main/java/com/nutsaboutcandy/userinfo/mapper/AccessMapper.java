package com.nutsaboutcandy.userinfo.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import com.nutsaboutcandy.userinfo.entity.Access;

@Mapper
public interface AccessMapper {

	@Select("INSERT INTO ACCESS (name) VALUES (#{name}) RETURNING id")
	@Options(useGeneratedKeys = true, keyColumn = "id", keyProperty ="id")
	long insert(Access access);
	
	@Select("SELECT * FROM ACCESS")
	@Results({ 
		@Result(column = "id", property = "id"),
		@Result(column = "name", property = "name")
	})
	List<Access> findAll();

	@Select("SELECT * FROM ACCESS where name = #{name}")
	@Results({ 
		@Result(column = "id", property = "id"),
		@Result(column = "name", property = "name")
	})
	Access findAccessName(String name);
	
}

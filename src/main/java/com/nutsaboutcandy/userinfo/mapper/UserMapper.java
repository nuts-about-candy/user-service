package com.nutsaboutcandy.userinfo.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import com.nutsaboutcandy.userinfo.entity.User;

@Mapper
public interface UserMapper {

	/**
	 * Find user by userName.
	 * @param username
	 * @return
	 */
	@Select("SELECT id, pass_word FROM USERS U "
			+ "WHERE U.user_name = #{username} ")
	@Results({ 
		@Result(column = "id", property = "id"),
		@Result(column = "pass_word", property = "password"),
	})
	User findUserByUserName(String username);
	
	/**
	 * Find User by id.
	 * @param id
	 * @return
	 */
	@Select("SELECT *, A.name as access FROM USERS U "
			+ "JOIN ACCESS A ON U.access_id=A.id "
			+ "WHERE U.id = #{id} ")
	@Results({ 
		@Result(column = "id", property = "id"),
		@Result(column = "user_name", property = "username"),
		@Result(column = "last_name", property = "lastName"),
		@Result(column = "first_name", property = "firstName"),
		@Result(column = "access_id", property = "accessId"),
		@Result(column = "access", property = "access"),
	})
	User findById(long id);
	
	
	/**
	 * Find all users.
	 * @return
	 */
	@Select("SELECT *, A.name as access FROM USERS U "
			+ "JOIN ACCESS A ON U.access_id=A.id")
	@Results({ 
		@Result(column = "id", property = "id"),
		@Result(column = "user_name", property = "userName"),
		@Result(column = "last_name", property = "lastName"),
		@Result(column = "first_name", property = "firstName"),
		@Result(column = "access_id", property = "accessId"),
		@Result(column = "access", property = "access"),
	})
	List<User> findAll();
	
	
	/**
	 * Insert user.
	 * @param user
	 * @return
	 */
	@Select("INSERT INTO USERS (user_name, pass_word, last_name, first_name, access_id) "
			+ "VALUES "
			+ "(#{username}, #{password}, #{lastName}, #{firstName}, #{accessId}) "
			+ "RETURNING id")
	@Options(useGeneratedKeys = true, keyColumn = "id", keyProperty ="id")
	long insert(User user);
}

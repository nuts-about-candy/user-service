package com.nutsaboutcandy.userinfo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.nutsaboutcandy.userinfo.entity.User;
import com.nutsaboutcandy.userinfo.service.UserService;

@RestController
public class UserController {

	@Autowired
	private UserService userService;
	
	@PostMapping("/signup")
	public ResponseEntity<Object> signup(@RequestBody User user) {
		
		System.out.println(user);
		try {
			userService.signUp(user);
		} catch(IllegalArgumentException ex) {
			return ResponseEntity.badRequest().body("{\"message\":\""+ ex.getMessage() +"\"}");
		} catch(RuntimeException ex) {
			ex.printStackTrace();
			return ResponseEntity.badRequest().body("{\"message\":\"Internal error occurred.\"}");
		}
		return ResponseEntity.ok().build();
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Object> getUserById(@PathVariable("id") long id) {
		
		User user = userService.getUserByUserId(id);
		
		return ResponseEntity.ok().body(user);
	}
	
	
	
}
